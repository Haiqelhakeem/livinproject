package Project;
import java.util.*;

public class Livin{
    public static void main(String[] args) {
        Bank bank = new Bank();
        int pilihan;                            // pilih menu
        int pilihan2;                           // pilihan masuk keluar aplikasi
        boolean flag = true;                    // keluar menu
        boolean flagOut = true;                 // flag keluar
        Scanner input = new Scanner(System.in); 
        Scanner in = new Scanner(System.in);

        String username;                        //UID yang dimasukkan oleh user untuk masuk ke akun
        String PIN;                             //PIN yang dimasukkan oleh user untuk masuk ke akun

        System.out.println("Selamat Datang");
        System.out.println("Silakan login");

        boolean loginFlag = false;                  //jika login flag true maka bisa masuk ke menu aplikasi
        User loggedinUser = null;                   //cek apakah ada user yang berhasil login
        List <User> userList = new ArrayList<>();   //untuk memasukkan user list
        
        //daftar user
        userList.add(new User("Haiqel", "123456", 1500000));
        userList.add(new User("Aziizul", "121212", 2000000));

        while (flagOut == true) {
            //input login
            System.out.println("1. Login");
            System.out.println("2. Keluar Aplikasi");
            System.out.print("Pilihan anda: ");
            pilihan2 = input.nextInt();

            if (pilihan2 == 1) {
                System.out.print("Masukkan username: ");
                username = in.nextLine();
                System.out.print("Masukkan pin: ");
                PIN = in.nextLine();

                //cek login pada daftar user
                for (User user : userList) {
                    if (user.getUID().equals(username)) {
                        if (user.getPIN().equals(PIN)) {
                            loggedinUser = user;
                            break;
                        }
                    }
                }

                //pesan login
                if (loggedinUser != null) {
                    System.out.println("Login berhasil");
                    System.out.println("Selamat datang " + loggedinUser.getUID());
                    loginFlag = true;
                } else {
                    System.out.println("Login gagal");
                    System.out.println("Apabila anda kesulitan login silakan hubungi: 082121374572\n");
                    loginFlag = false;
                }

                while (loginFlag == true) {
                    System.out.println("-----MENU-----");
                    System.out.println("1. Transfer");
                    System.out.println("2. Cek Saldo");
                    System.out.println("3. Tarik Tunai");
                    System.out.println("4. Bayar Tagihan");
                    System.out.println("5. Isi Pulsa");
                    System.out.println("6. Keluar");
    
                    do {
                        System.out.print("Masukkan pilihan anda: ");
                        pilihan = input.nextInt();
                        switch (pilihan) {    
                            case 1:
                                System.out.println("====TRANSFER====");
                                bank.transfer(username);
                                break;
                            
                            case 2:
                                System.out.println("====CEK SALDO====");
                                bank.getSaldo(username);
                                break;
                            
                            case 3:
                                System.out.println("====TARIK TUNAI====");
                                bank.tarikTunai(username);
                                break;

                            case 4:
                                System.out.println("====BAYAR TAGIHAN====");
                                bank.bayar(username);
                                break;
                            
                            case 5:
                                System.out.println("====ISI PULSA====");
                                bank.isiPulsa(username);
                                break;
    
                            case 6:
                                System.out.println("Keluar..");
                                flag = false;
                                loginFlag = false;
                                break;
                        
                            default:
                                break;
                        }
                    } while (flag == true);
                }

            } else if (pilihan2 == 2) {
                flagOut = false;
            } else{
                System.out.println("Pilihan anda invalid");
            }
        }  
    }
}
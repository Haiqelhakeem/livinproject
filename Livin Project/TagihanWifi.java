package Project;

//inheritance
public class TagihanWifi extends Tagihan {
    public TagihanWifi(String namaTagihan, int totalTagihan){
        super(namaTagihan, totalTagihan);
    }

    //penerapan polymorphism override
    @Override
    public int biayaAdmin() {
        return 3500;
    }
}

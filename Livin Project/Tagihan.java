package Project;

//abstraction
public abstract class Tagihan {
    //encapsulation
    private String namaTagihan;
    protected int totalTagihan;

    //constructor
    public Tagihan(String namaTagihan, int totalTagihan){
        this.namaTagihan = namaTagihan;
        this.totalTagihan = totalTagihan;
    }

    //getter nama tagihan
    public String getNamaTagihan(){
        return namaTagihan;
    }

    //getter biaya tagihan yang harus dibayar
    public int getTotalTagihan(){
        return totalTagihan;
    }

    //polymorphism untuk menambahkan biaya admin yang
    //berbeda pada tiap pembayaran tagihan
    public abstract int biayaAdmin();
}

package Project;

//inheritance
public class TagihanListrik extends Tagihan {
    public TagihanListrik(String namaTagihan, int totalTagihan){
        super(namaTagihan, totalTagihan);
    }

    //penerapan polymorphism override
    @Override
    public int biayaAdmin() {
        return 2000;
    }
}

package Project;

//inheritance
public class TagihanAir extends Tagihan{
    public TagihanAir(String namaTagihan, int totalTagihan){
        super(namaTagihan, totalTagihan);
    }

    //penerapan polymorphism override
    @Override
    public int biayaAdmin() {
        return 1500;
    }
}

package Project;
public class User {
    private String UIDuser;
    private String PINuser;
    public int saldo;

    //constructor
    public User(String UIDuser, String PINuser, int saldo){
        this.PINuser = PINuser;
        this.UIDuser = UIDuser;
        this.saldo = saldo;
    }

    //getter UID akun
    String getUID(){
        return UIDuser;
    }

    //getter PIN akun
    String getPIN(){
        return PINuser;
    }
}

# Link Source Code
Link code: https://github.com/Haiqelhakeem/UAS-PrakPBO-Laravel


# 1. Use Case
>Sisi User

| No | Use case | Actor | Priority | Status |
|----|----------|-------|----------|--------|
| 1 | Sebagai user, saya ingin bisa masuk ke aplikasi | User | High | Completed |
| 2 | Sebagai user, saya bisa menggunakan fitur transfer dari akun saya ke akun User lain | User | High | Completed |
| 3 | Sebagai user, saya bisa menggunakan fitur cek saldo untuk melihat sisa saldo di akun rekening saya | User | High | Completed |
| 4 | Sebagai user saya bisa melihat riwayat transaksi | Actor | High | Completed |
| 5 | Sebagai user, saya bisa keluar dari aplikasi | User | High | Completed |
| 6 | Sebagai user, saya bisa menggunakan fitur bayar tagihan yang ada pada akun rekening saya | User | High | Incomplete |
| 7 | Sebagai user saya dapat memblokir kartu milik saya | User | High | Incomplete | 
| 8 | Sebagai user, saya bisa menggunakan fitur isi pulsa pada aplikasi dengan memasukkan nomor HP | User | Medium | Incomplete |
| 9 | Sebagai user saya dapat mengubah profil saya | User | Medium | Incomplete | 
| 10 | Sebagai user saya dapat menerima notifikasi | User | Medium | Incomplete |

> Sisi Admin

| No | Use Case | Actor | Priority | Status |
|----|----------|-------|----------|--------|
| 1 | Sebagai admin saya dapat mengelola pengguna pengguna | Admin | High | Incomplete |
| 2 | Sebagai admin saya dapat mengelola permintaan pembukaan rekening | Admin | High | Incomplete |
| 3 | Sebagai admin saya dapat verifikasi dan mengatur transaksi | Admin | High | Incomplete | 
| 4 | Sebagai admi saya dapat memberi notifikasi kepada user | Admin | Medium | Incomplete | 

>Sisi Manajemen

| No | Use Case | Actor | Priority | Status |
|----|----------|-------|----------|--------|
| 1 | Sebagai manager saya dapat menganalisis data pengguna untuk memahami kebutuhan pengguna | Manager | High | Incomplete |
| 2 | Sebagai manager saya dapat melakukan promosi atau pemasaran pada aplikasi | Manager | High | Incomplete |

# 2. Class Diagram
![class-diagram](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/Livin-project-img/Class_diagram_livin.drawio.png)

# 3. SOLID Design Principle
a. Single Responsibility
>Memisahkan fungsi-fungsi yang berbeda ke dalam kelas-kelas yang berbeda. Contoh penerapannya adalah pada anak class Controller yang memiliki fungsi berbeda-beda seperti login controller untuk autentikasi login, transfer controller untuk mengatur fitur transfer.

b. Open/Closed
>Terbuka untuk menambahkan fitur tanpa mengubah kode yang ada. Contoh penerapannya adalah di waktu yang akan datang dalam PaymentController dimana kita bisa menambahkan metoe pembayaran contoh dari kartu kredit bisa ditambahkan dengan e-wallet

c. Liskov Substitution
>Fitur inheritance, dimana objects dari superclass harus bisa direplace pada object subclasses tanpa menyebabkan masalah. Jadi objects dari subclasses harus memiliki behaviour yang sama dengan superclass. Contoh penerapannya adalah class Controller sebagai superclass dan anak class lain seperti LoginController, DashboardController, dll.

d. Interface Segregation
>Membagi Interface menjadi bagian yang lebih kecil dan lebih spesifik, prinsip Interface Segregation membantu dalam mengurangi ketergantungan yang tidak perlu dan membuat kode lebih fleksibel.

e. Dependency Inversion
>Ketergantungan antara satu instansi class dengan instansi class yang lain. Contoh penerapannya adalah dalam class TransferController dengan metode localTransfer() dan transferForm(), menggunakan Auth::user() untuk mendapatkan informasi pengguna yang sedang terautentikasi. Penggunaan Auth ini adalah contoh dari injeksi ketergantungan melalui konstruktor. 

# 4. Design Pattern

Design pattern MVC (Model-View-Controller) adalah pola desain yang digunakan dalam pengembangan perangkat lunak untuk memisahkan komponen-komponen utama aplikasi menjadi tiga bagian yang berbeda: Model, View, dan Controller. Setiap bagian memiliki tanggung jawab yang jelas dalam aplikasi.

Model:
>1. Model mewakili data dan logika bisnis aplikasi.
>2. Tanggung jawab utama model adalah mengelola dan memanipulasi data.
>3. Model seringkali berinteraksi dengan sumber data eksternal, seperti database, API, atau file.
>4. Model juga mengimplementasikan logika bisnis dan aturan validasi terkait data.

View:
>1. View bertanggung jawab untuk menampilkan informasi kepada pengguna.
>2. View tidak berisi logika bisnis atau manipulasi data.
>3. Tugas utama view adalah mengatur tampilan dan presentasi data agar mudah dipahami oleh pengguna.
>4. View biasanya menerima data dari model untuk ditampilkan kepada pengguna.

Controller:
>1. Controller berperan sebagai penghubung antara model dan view.
>2. Tanggung jawab utama controller adalah menangani permintaan dari pengguna.
>3. Controller menerima input dari pengguna, memprosesnya, dan melakukan pemutakhiran pada model jika diperlukan.
>4. Controller juga menentukan tampilan mana yang akan ditampilkan kepada pengguna berdasarkan aksi atau permintaan yang diterima.

# 5. Database
Code telah tersambung ke dalam database livinproject_db melalui pengaturan dalm file .env
![database-connection](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/Livin-project-img/Screenshot_2023-07-05_225244.png)

Contoh pendeklarasian atribut tabel dalam anak class model
![database-connection-2](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/Livin-project-img/Database-connection.gif)

Database dalam seeder

![database-connection-3](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/Livin-project-img/Screenshot_2023-07-05_232207.png)

# 6. Web Service
Menggunakan routing yang telah diatur dalam controller sehingga dapat menggunakan web untuk menjalankannya
![web-service](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/Livin-project-img/Web-service.gif)

# 7. Graphical User Interface
Memiliki tampilan utama dashboard yang terdiri dari beberapa tombol, dimana tombol saat diklik menjalankan fungsi tertentu
![GUI](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/Livin-project-img/GUI.gif)

# 8. HTTP GUI
Menggunakan blade sebagai GUI dalam laravel. Digunakan dalam membuat login page, dashboard, dan history page.
![HTTP-GUI](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/Livin-project-img/HTTP-GUI.gif)

# 9. Youtube
Link demo youtube: https://youtu.be/L36zdgz9TVE

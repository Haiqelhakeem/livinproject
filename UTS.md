# Link Source Code
[Livin](https://gitlab.com/Haiqelhakeem/livinproject/-/tree/main/Livin%20Project)

# 1 Pendekatan Matematika
Pendekatan matematika untuk menghitung saldo yang dikurangi dalam transaksi transfer

![pendekatan-matematika](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/GIF%20Program/Pendekatan_mat.gif)

# 2 Algoritma Program
Algoritma

![algoritma](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/GIF%20Program/Algoritma.gif)

# 3 Konsep-Konsep Dasar OOP
Konsep dasar OOP

![konsep-oop](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/GIF%20Program/Konsep_OOP.gif)

# 4 Encapsulation
Encapsulation dalam file tagihan agar bisa get nama tagihan dan biaya tagihan

![encapsulation](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/GIF%20Program/Encapsulation.gif)

# 5 Abstraction
Abstraction class Tagihan karena class tagihan masih belum jelas tagihan jenis apa sehingga dijadikan class induk

![abstraction](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/GIF%20Program/Abstraction.gif)

# 6 Inheritance dan Polymporphism
Inheritance digunakan untuk mewarisi class induk ke class anak. Yakni dari class Tagihan ke class Tagihan Wifi, Tagihan Air, Tagihan Listrik.
Polymorphism digunakan untuk override method biaya admin yang berbeda-beda

![inheritance-polymorphism](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/GIF%20Program/Inheritance_and_Polymorphism.gif)

# 7 Use Case

>Sisi User
1. Use Case: Login
>**User mampu masuk ke aplikasi setelah terdaftar pada aplikasi**\
a. User memilih bagian login aplikasi\
b. User memasukkan username dan pin sesuai akun yang digunakan.\
c. Sistem memverifikasi username dan pin ke database\
d. User berhasil masuk jika username dan pin benar, user tidak gagal masuk jika username atau pin salah.

2. Use Case: Transfer
>**User mampu mengirimkan uang dari rekeningnya ke rekening orang lain**\
a. User memilih menu transfer pada aplikasi\
b. User memasukkan UID tujuan\
c. User memasukkan jumlah yang akan ditransfer\
d. User memasukkan pin untuk mengkonfirmasi transaksi\
e. Sistem akan memberi pesan apakah transaksi berhasil atau gagal

3. Use Case: Cek Saldo
>**User mampu melihat sisa saldo pada rekeningnya**\
a. User memilih menu cek saldo\
b. Sistem menampilkan jumlah saldo yang ada di rekeningnya

4. Use Case: Bayar Tagihan
>**User mampu membayar berbagai macam tagihan melalui aplikasi**\
a. User memilih menu bayar tagihan\
b. User memilih tagihan yang akan dibayar\
c. Sistem akan memberikan pesan jika tagihan sudah dibayar\
d. Sistem menampilkan biaya tagihan yang harus dibayar\
e. User mengonfirmasi pembayaran\
f. Sistem memberikan pesan transaksi berhasil atau tidak

5. Use Case: Isi Pulsa
>**User mampu mengisi pulsa pada nomor yang dimasukkan melalui aplikasi**\
a. User memilih menu isi pulsa\
b. User memasukkan jumlah pulsa ingin dibeli sesuai yang ditampilkan di menu\
c. Sistem memberikan pesan transaksi berhasil atau tidak

6. Use Case: Logout
>**User mampu keluar dari aplikasi**\
a. User memilih menu keluar\
b. Sistem mengeluarkan user dari menu dan memberikan pilihan keluar dari aplikasi atau login lagi\
c. User memilih keluar dari aplikasi\
d. Sistem mengeluarkan user dari aplikasi

7. Use Case: Aktivitas transaksi

8. Use Case: Ubah Profil

9. Use Case: Blokir Kartu

10. Use Case: Bayar via QRis

# 8 Use Case Table dan Diagram
>Use Case Table

| No | Use case | Actor | Priority | Status |
|----|----------|-------|----------|--------|
| 1 | Sebagai user, saya ingin bisa masuk ke aplikasi | User | High | Completed |
| 2 | Sebagai user, saya bisa menggunakan fitur transfer dari akun saya ke akun User lain | User | High | Completed |
| 3 | Sebagai user, saya bisa menggunakan fitur cek saldo untuk melihat sisa saldo di akun rekening saya | User | High | Completed |
| 4 | Sebagai user, saya bisa menggunakan fitur bayar tagihan yang ada pada akun rekening saya | User | High | Completed |
| 5 | Sebagai user, saya bisa keluar dari aplikasi | User | High | Completed |
| 6 | Sebagai user, saya bisa menggunakan fitur isi pulsa pada aplikasi dengan memasukkan nomor HP | User | Medium | Completed |

# 9 Demo App di Youtube
>[link demo app](https://youtu.be/v99ivQkXSSY)

# 10 UX Aplikasi
>Tampilan masuk aplikasi\
![tampilan-awal](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/UX%20Program/Screenshot_2023-05-13_153940.png)

>Menu\
![menu](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/UX%20Program/Screenshot_2023-05-13_153957.png)

>Fitur Transfer\
![transfer](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/UX%20Program/Screenshot_2023-05-13_154023.png)

>Cek Saldo\
![cek-saldo](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/UX%20Program/Screenshot_2023-05-13_154036.png)

>Tarik Tunai\
![tarik-tunai](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/UX%20Program/Screenshot_2023-05-13_154101.png)

>Bayar Tagihan\
![bayar-tagihan](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/UX%20Program/Screenshot_2023-05-13_154123.png)

>Isi Pulsa\
![isi-pulsa](https://gitlab.com/Haiqelhakeem/livinproject/-/raw/main/UX%20Program/Screenshot_2023-05-13_154148.png)
